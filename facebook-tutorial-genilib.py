#!/usr/bin/env python

import geni.portal as portal
import geni.rspec.pg as RSpec
import geni.rspec.igext as IG
from lxml import etree as ET
import geni.urn as URN

#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    EPCIMG = PN.PNDEFS.DEF_BINOEPC_IMG
    UE_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    ENODEB_HWTYPE = "enodeb"
    UE_HWTYPE = "nexus5"

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()



#
# Describe profile.  Will be rendered in Markdown on Portal.
#
tourDescription = \
"""
###*<center>Use this profile to instantiate real OTS hardware paired with OpenEPC.</center>*
---

Attributes/features:

* One real OTS UE handset (Google Nexus5)
* One real OTS eNodeB (ip.access e40)
* Emulated RF link between OTS devices (set to 0dB attenuation)
* Essential EPC compenents (OpenEPC): HSS, MME, SGW, PGW
* Configurable number of emulated edge components: UEs and eNodeBs.

"""

tourInstructions = \
"""
This profile makes use of user-supplied parameters. You can use these parameters to tune the number of clients (emulated UEs), request additional emulated eNodeBs, and to choose the hardware used.  An advanced parameter allows you to set the default LAN bandwidth.

This is a parameterized profile implemented via a [geni-lib](http://geni-lib.readthedocs.org "geni-lib documentation") script. You may make a copy of the script to use in your own profile where you can modify the script to suit your needs.
"""

#
# Setup the Tour info with the above description and instructions.
#  
tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN,tourDescription)
tour.Instructions(IG.Tour.MARKDOWN,tourInstructions)
request.addTour(tour)


pc.defineParameter("HWTYPE","Node Hardware Type",
                   portal.ParameterType.STRING, "pc",
                   [("pc","Any available (compatible) physical machine type"),
                    ("pc3000","Emulab pc3000 nodes"),
                    ("d710","Emulab d710 nodes"),
                    ("pcvm","Any available (compatible) virtual machine type"),
                    ("pc3000vm","Virtual machines on top of pc3000 nodes."),
                    ("d710vm","Virtual machines on top of d710 nodes.")],
                   longDescription="Specify which node resource type to use for OpenEPC nodes. Note that only those types that are compatible with the OpenEPC image(s) are listed.")

pc.defineParameter("LINKBW","Default Link Bandwidth (Mbps)",
                   portal.ParameterType.INTEGER, 1000,
                   longDescription="Specify the default LAN bandwidth in Mbps for all EPC LANs. Leave at \"0\" to indicate \"best effort\". Values that do not line up with common physical interface speeds (e.g. 10, 100, or 1000) WILL cause the insertion of link shaping elements.",
                   advanced=True)

pc.defineParameter("FIXEDUE", "Bind to a specific UE",
                   portal.ParameterType.STRING, "",
                   longDescription="Input the name of a PhantomNet UE node to allocate (e.g., \'ue1\').  Leave blank to let the mapping algorithm choose.",
                    advanced=True)
pc.defineParameter("FIXEDENB", "Bind to a specific eNodeB",
                   portal.ParameterType.STRING, "",
                   longDescription="Input the name of a PhantomNet eNodeB device to allocate (e.g., \'enodeb01\').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.",
                    advanced=True)

#
# Get any input parameter values that will override our defaults.
#
params = pc.bindParameters()

#
# Verify parameters and throw errors.
#
if int(params.LINKBW) not in [0, 10, 100, 1000]:
    pwarn = portal.ParameterWarning("You are asking for a default link bandwidth that is NOT a standard physical link speed. Link shaping resources WILL be inserted!", ['LINKBW'])
    pc.reportWarning(pwarn)
    pass

# XXX: put in check for fixed eNB and UE device names.

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Scale link bandwidth parameter to kbps
#
params.LINKBW *= 1000

#
# Set the hardware and image for the epc node factory function
#
PN.EPCNodeFactorySettings.hardware_type = params.HWTYPE
PN.EPCNodeFactorySettings.disk_image = GLOBALS.EPCIMG

#
# Create the lans we need
#
mgmt = request.EPClan(PN.EPCLANS.MGMT)
net_a = request.EPClan(PN.EPCLANS.NET_A)
net_a.bandwidth = params.LINKBW
net_b = request.EPClan(PN.EPCLANS.NET_B)
net_b.bandwidth = params.LINKBW
net_d = request.EPClan(PN.EPCLANS.NET_D)
net_d.bandwidth = params.LINKBW


#
# Add the core EPC nodes
#

# epc-enablers node
epcen = PN.mkepcnode("epc", PN.EPCROLES.ENABLERS, request = request)
mgmt.addMember(epcen)
net_a.addMember(epcen)

# pgw node
pgw = PN.mkepcnode("pgw", PN.EPCROLES.PGW, request = request)
mgmt.addMember(pgw)
net_a.addMember(pgw)
net_b.addMember(pgw)

# sgw-mme-sgsn node
sgw = PN.mkepcnode("sgw", PN.EPCROLES.SGW_MME_SGSN, request = request)
mgmt.addMember(sgw)
net_b.addMember(sgw)
net_d.addMember(sgw)


# Add a node to act as the ADB target host
adb_t = request.RawPC("adb-tgt")
adb_t.disk_image = GLOBALS.ADB_IMG


# Add a real eNodeB
renb1 = request.eNodeB("renb1")
if params.FIXEDENB:
    renb1.component_id = params.FIXEDENB
renb1.hardware_type = GLOBALS.ENODEB_HWTYPE
net_d.addMember(renb1)
net_d.bandwidth = 1000 * 1000 # Hack: must set bw on net_d to 100Mbps.
renb1_rflink1 = renb1.addInterface("renb1_rflink1")
renb1_rflink2 = renb1.addInterface("renb1_rflink2")

# Add the first real UE
rue1 = request.UE("rue1")
if params.FIXEDUE:
    rue1.component_id = params.FIXEDUE
rue1.hardware_type = GLOBALS.UE_HWTYPE
rue1.disk_image = GLOBALS.UE_IMG
rue1.adb_target = "adb-tgt"
rue1_rflink1 = rue1.addInterface("rue1_rflink1")


# Create the RF link between the real UE and eNodeB
rflink1 = request.RFLink("rflink1")
rflink1.addInterface(rue1_rflink1)
rflink1.addInterface(renb1_rflink1)

# Add the second real UE
rue2 = request.UE("rue2")
if params.FIXEDUE:
    rue2.component_id = params.FIXEDUE
rue2.hardware_type = GLOBALS.UE_HWTYPE
rue2.disk_image = GLOBALS.UE_IMG
rue2.adb_target = "adb-tgt"
rue2_rflink1 = rue2.addInterface("rue2_rflink1")


# Create the RF link between the real UE and eNodeB
rflink2 = request.RFLink("rflink2")
rflink2.addInterface(rue2_rflink1)
rflink2.addInterface(renb1_rflink2)


#
# Print and go!
#
pc.printRequestRSpec(request)