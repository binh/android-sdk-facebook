package com.testBob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.robotium.solo.Condition;
import com.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


@SuppressWarnings("unchecked")
public class Bob extends ActivityInstrumentationTestCase2 {
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.calculator.Main";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.activity.FrameworkBasedFbFragmentChromeActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.LoginActivity";
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.activity.FbMainTabActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.composer.activity.ComposerActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.android.music";
	private static Class launcherActivityClass;
	private static final String LOG_TAG = "BOB PhantomNet:";
	private final ArrayList<String> KEYS = new ArrayList<String>();
	private static final int REPEATITION = 2; // number of posts.
	private final ArrayList<String> FOUND_KEYS = new ArrayList<String>();

	private void initKeys(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		for (int i = 1; i <= REPEATITION; i++){
			KEYS.add(dateFormat.format(date)+i);
		}
	}
	
	static {
		try {
			launcherActivityClass = Class .forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		} catch (ClassNotFoundException e) { 
			throw new RuntimeException(e);
		}
	}
	
	private void waitForViewDisappear(String viewName){
		final View view = solo.getView(viewName);
	
	  solo.waitForCondition(new Condition() {
	      @Override
	      public boolean isSatisfied() {
	          return !view.isShown();
	      }
	  }, 5000);
	}
	
	public Bob() throws ClassNotFoundException { 
		super(launcherActivityClass);
	}

	private Solo solo;
	@Override
	protected void setUp() throws Exception {
		initKeys();
		solo = new Solo(getInstrumentation(), getActivity());
	}
	
	
	public void testSpinningForStatus() {
		solo.waitForActivity("com.facebook.katana.activity.FbMainTabActivity");

		//for (int i=0; i<=50; i++){
		while (true){
			solo.drag(500, 500, 500, 1000, 10);
			this.waitForViewDisappear("pull_to_refresh_spinner");
			TextView newFeed = (TextView)solo.getView("feed_story_message");
			String newFeedText = newFeed.getText().toString();
			while (!solo.getView("feed_story_image_attachment").isShown()){
				Log.i(LOG_TAG,"Waiting for photo to show ...");
			}
			//Log.i(LOG_TAG,"FOUND_KEYS = "+FOUND_KEYS.toString());
			if (!FOUND_KEYS.contains(newFeedText)){ //new feed.
				FOUND_KEYS.add(newFeedText);
				Log.i(LOG_TAG,"AT@ "+System.nanoTime()+" FETCHED: " + newFeedText);
			}
			Log.i(LOG_TAG,"Refreshing, no new feed found ... ");
		}
	}
	
	
	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}
}