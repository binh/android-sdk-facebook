package com.testAlice;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.robotium.solo.Condition;
import com.robotium.solo.Solo;

import android.R;
import android.app.Instrumentation;
import android.os.Bundle;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestRunner;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;


@SuppressWarnings("unchecked")
public class Alice extends ActivityInstrumentationTestCase2 {
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.calculator.Main";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.activity.FrameworkBasedFbFragmentChromeActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.LoginActivity";
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.activity.FbMainTabActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.composer.activity.ComposerActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.android.music";
	private static Class launcherActivityClass;
	private static final String TEXT_TO_POST = "HELLO WORLD";
	private static final String KEY_FILE = "keys.txt";
	private static final String LOG_TAG_FILE = "log_tag.txt";
	private static String LOG_TAG = "ALICE PhantomNet ";
	private static int REPEATITION = 0; // number of posts.
	private static int GAP_TIME = 80; //number of pause, each is ~2s on nexus 7.
	private ArrayList<String> KEYS = new ArrayList<String>();
	
	private void initKeys(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		for (int i = 1; i <= REPEATITION; i++){
			KEYS.add(dateFormat.format(date)+i);
		}
		KEYS = this.readKeysFromSdcard(KEY_FILE);
		Log.i(LOG_TAG,"Read status and log tag from file ...");
		LOG_TAG = LOG_TAG + "[ " + this.readKeysFromSdcard(LOG_TAG_FILE).get(0) + " ]:";
		REPEATITION = KEYS.size();
	}
	
	
	static {
		try {
			launcherActivityClass = Class .forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		} catch (ClassNotFoundException e) { 
			throw new RuntimeException(e);
		}
	}
	
	private ArrayList<String> readKeysFromSdcard(String fileName){
		File sdcard = Environment.getExternalStorageDirectory();

		//Get the text file
		File file = new File(sdcard,fileName);
	
		//Read text from file
		String text = "";
	
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;
	
		    while ((line = br.readLine()) != null) {
		        text+=line;
		    }
		    br.close();
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
		}
		//Log.d(LOG_TAG,"String from file = "+text);
		List<String> keys = new ArrayList<String>(Arrays.asList(text.split("\\s+")));
		return (ArrayList<String>) keys;
	}
	
	public Alice() throws ClassNotFoundException { 
		super(launcherActivityClass);
	}

	private Solo solo;

	@Override
	protected void setUp() throws Exception {
		initKeys();
		solo = new Solo(getInstrumentation(), getActivity());
	}


	
	private void waitViewToShow(String viewName){
		try{
				final View view = solo.getView(viewName);
			  solo.waitForCondition(new Condition() {
		      @Override
		      public boolean isSatisfied() {
		          return !view.isShown();
		      }
			  }, 10000);
		} catch (Exception e){
			Log.d(LOG_TAG, "Exception: View "+viewName+"not found. Pass");
		}
	}

	private void waitForViewDisappear(final String viewName){
		try {
			final View view = solo.getView(viewName);
			solo.waitForView(view);
			Log.d(LOG_TAG,"View "+ viewName+" appeared...");
			Log.d(LOG_TAG,"Waiting for " + viewName + " to disappear...");
		  solo.waitForCondition(new Condition() {
		      @Override
		      public boolean isSatisfied() {
		    			Log.i(LOG_TAG,"Waiting for view " + viewName+ " to disappear ... ");
		          return !view.isShown();
		          //return !view.isAttachedToWindow();
		      }
		  }, 500000);
		} catch (AssertionError e){
			Log.d(LOG_TAG, "Past assertion: View "+viewName+" was not there...");
		}
	}
	
	public void testPostStatus() {
		
	  
		String textToPost = ""; 
		int postPhoto = 1;
		Log.d(LOG_TAG,"View "+ REPEATITION);
		for (int i=0; i<REPEATITION; i++){
			//Log.i(LOG_TAG,"1");
			//solo.waitForActivity("com.facebook.katana.activity.FbMainTabActivity");
			solo.waitForText("STATUS",1,1000);
			solo.waitForView(solo.getView("publisher_button0"));
			//this.waitViewToShow("publisher_button0");
			Log.i(LOG_TAG,"START TESTING....");
		
		
			//solo.clickOnText("STATUS");
			solo.clickOnView(solo.getView("publisher_button0"));
			Log.i(LOG_TAG,"CLICKED ON STATUS....");

			//textToPost = TEXT_TO_POST +" "+System.currentTimeMillis();
			textToPost = this.KEYS.get(i);

			if (postPhoto == 0){
				solo.waitForActivity("com.facebook.composer.activity.ComposerActivity");
				solo.enterText((EditText) solo.getView("status_text"), "");
				solo.enterText((EditText) solo.getView("status_text"), textToPost);
				Log.i(LOG_TAG,"ENTERED TEXT...");
			} else {
				//Log.i(LOG_TAG,"5");
				//solo.waitForActivity("com.facebook.composer.activity.ComposerActivity");
				solo.sleep(500);
				solo.waitForView(solo.getView("add_photo"));
				//this.waitViewToShow("add_photo");
				//solo.sleep(100);
				solo.clickOnView(solo.getView("add_photo"));
				solo.sleep(500);
				//this.waitViewToShow("image");
				solo.waitForText("Camera Roll",1,3000);
				solo.waitForView(solo.getView("back_button"));
				solo.waitForView(solo.getView("image"));
				//solo.waitForText("Camera Roll");
				solo.clickOnView(solo.getView("image"));
				solo.sleep(500);
				//solo.waitForText("DONE");
				solo.waitForView(solo.getView("done_button"));
				//solo.waitForText("DONE",1,3000);
				solo.clickOnView(solo.getView("done_button"));
				solo.sleep(500);
				//solo.sleep(100);
				Log.i(LOG_TAG,"PHOTO ADDED...");
				solo.waitForDialogToClose();

				//solo.waitForActivity("com.facebook.composer.activity.ComposerActivity");
				Log.i(LOG_TAG,"activity loaded...");
				//solo.waitForText("Say something about this photo",1,50000);
				//solo.sleep(100);
				//solo.clickOnText("Say something about this photo");
				solo.waitForView(solo.getView("status_text"));
				Log.i(LOG_TAG,"status_text loaded...");
				solo.enterText((EditText) solo.getView("status_text"), "");
				solo.enterText((EditText) solo.getView("status_text"), textToPost);
				Log.i(LOG_TAG,"PHOTO DESCRIPTION ADDED...");
			}
			
			
			//solo.waitForText("Public");
			solo.sleep(500);
			long startTime = System.nanoTime();
			solo.clickOnView(solo.getView("composer_primary_named_button"));
			//waitForViewDisappear("progress_horizontal");
			
			Log.i(LOG_TAG,"AT@0 "+System.nanoTime()+" CICKED ON POST...");
			//solo.waitForDialogToClose();
			//solo.waitForView(solo.getView("progress_horizontal"));
			this.waitForViewDisappear("progress_horizontal");
			//this.waitForViewDisappear("empty_layout");
			//solo.waitForActivity("com.facebook.katana.activity.FbMainTabActivity");
			//solo.waitForText(textToPost);
			//solo.waitForView(solo.getView("loading_view"));
			//this.waitForViewDisappear("empty_layout");
			/*
			while (solo.waitForView(solo.getView("progress_horizontal")));
			{
				Log.i(LOG_TAG,"Wait for the bar...");
			}
			*/
			
			long endTime = System.nanoTime();
			Log.i(LOG_TAG,"Progress bar disappeared...");

			
			//solo.clickOnView(postView);
	
			Log.i(LOG_TAG,"POST "+ i + " complete: "+ (endTime - startTime)/1000000000 + " ms");
			Log.i(LOG_TAG,"AT@1 "+System.nanoTime()+" POSTED: "+ textToPost);
			if (i == REPEATITION-1){
				GAP_TIME = 90;
			}
			for (int j=0; j <=GAP_TIME; j++){
				solo.clickOnView(solo.getView("news_feed_tab")); //preventing the device to sleep.
				solo.sleep(1000);
				solo.clickOnView(solo.getView("news_feed_tab")); //preventing the device to sleep.
				Log.d(LOG_TAG,"Sleeping before the next post ... "+ j);
			}
		}
	}
	
	
	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}
}
