package com.testcalculator;

import android.os.Bundle;
import android.test.InstrumentationTestRunner;

public class MyTestRunner extends InstrumentationTestRunner {

  private String mArgument;

  /* (non-Javadoc)
   * @see android.test.InstrumentationTestRunner#onCreate(android.os.Bundle)
   */
  @Override
  public void onCreate(Bundle arguments) {
      super.onCreate(arguments);

      if (arguments != null) {
          mArgument = arguments.getString("myarg");
      }
  }

  public String getArgument() {
      return mArgument;
  }

}