package com.testBobNoRefresh;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.robotium.solo.Condition;
import com.robotium.solo.Solo;

import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


@SuppressWarnings("unchecked")
public class BobNoRefresh extends ActivityInstrumentationTestCase2 {
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.calculator.Main";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.activity.FrameworkBasedFbFragmentChromeActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.LoginActivity";
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.katana.activity.FbMainTabActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.facebook.composer.activity.ComposerActivity";
	//private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "com.android.music";
	private static Class launcherActivityClass;
	private static String LOG_TAG = "BOB PhantomNet:";
	private static final String KEY_FILE = "keys.txt";
	private ArrayList<String> KEYS = new ArrayList<String>();
	private static int REPEATITION = 0; // number of posts.
	private final ArrayList<String> FOUND_KEYS = new ArrayList<String>();
	private static final String LOG_TAG_FILE = "log_tag.txt";

	private void initKeys(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		for (int i = 1; i <= REPEATITION; i++){
			KEYS.add(dateFormat.format(date)+i);
		}
		KEYS = this.readKeysFromSdcard(KEY_FILE);
		Log.i(LOG_TAG,"Read status and log tag from file ...");
		LOG_TAG = LOG_TAG + "[ " + this.readKeysFromSdcard(LOG_TAG_FILE).get(0) + " ]:";
		REPEATITION = KEYS.size();
	}
	
	private ArrayList<String> readKeysFromSdcard(String fileName){
		File sdcard = Environment.getExternalStorageDirectory();

		//Get the text file
		File file = new File(sdcard,fileName);
	
		//Read text from file
		String text = "";
	
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;
	
		    while ((line = br.readLine()) != null) {
		        text+=line;
		    }
		    br.close();
		}
		catch (IOException e) {
		    //You'll need to add proper error handling here
		}
		//Log.d(LOG_TAG,"String from file = "+text);
		List<String> keys = new ArrayList<String>(Arrays.asList(text.split("\\s+")));
		return (ArrayList<String>) keys;
	}
	
	static {
		try {
			launcherActivityClass = Class .forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		} catch (ClassNotFoundException e) { 
			throw new RuntimeException(e);
		}
	}
	
	private void waitForViewDisappear(String viewName){
		final View view = solo.getView(viewName);
	
	  solo.waitForCondition(new Condition() {
	      @Override
	      public boolean isSatisfied() {
	          return !view.isShown();
	      }
	  }, 5000);
	}
	
	public BobNoRefresh() throws ClassNotFoundException { 
		super(launcherActivityClass);
	}

	private Solo solo;
	@Override
	protected void setUp() throws Exception {
		initKeys();
		solo = new Solo(getInstrumentation(), getActivity());
	}
	
	private boolean isFoundAllKeys(){
		for (int i=0; i< KEYS.size();i++){
			if (!FOUND_KEYS.contains(KEYS.get(i))){
				return false;
			}
		}
		return true;
	}
	public void testSpinningForStatus() {
		//solo.waitForActivity("com.facebook.katana.activity.FbMainTabActivity");

		//for (int i=0; i<=50; i++){
		Log.i(LOG_TAG,"FOUND_KEYS = "+FOUND_KEYS.toString() + "REPETITION = "+REPEATITION);

		while (!this.isFoundAllKeys()){ //Repeat until all keys are fetched
			//solo.drag(500, 500, 500, 1000, 10);
			//this.waitForViewDisappear("pull_to_refresh_spinner");
			solo.clickOnView(solo.getView("news_feed_tab")); //preventing the device to sleep.
			TextView newFeed = (TextView)solo.getView("feed_story_message");
			String newFeedText = newFeed.getText().toString();
			while (!solo.getView("feed_story_image_attachment").isShown()){
				Log.i(LOG_TAG,"Waiting for photo to show ...");
			}
			if (!FOUND_KEYS.contains(newFeedText)){ //new feed.
				FOUND_KEYS.add(newFeedText);
				Log.i(LOG_TAG,"AT@1 "+System.nanoTime()+" FETCHED: " + newFeedText);
			}
			Log.i(LOG_TAG,"Refreshing, no new feed found ... ");
			solo.sleep(1);
		}
	}
	
	
	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
	}
}