#while IFS= read -r line
while read line
do 
	if [[ $line == *"ALICE"* ]]; 
	then 
		printf '%s: %s\n' "$(gdate '+%s%N' | cut -b1-13)" "$line" 
	fi 
done < keys.txt
