#!/bin/bash

source constants.sh

echo "Gathering results ..."

keys=$(cat keys.txt)

for key in $keys
do
	a1=$(grep "AT$" alice.logcat | grep $key | awk -F":" '{print $1}')
	a2=$(grep "AT@" alice.logcat | grep $key | awk -F":" '{print $1}')
	b=$(grep "AT@" bob.logcat | grep $key | awk -F":" '{print $1}')
	delta_fetch=$(echo $b-$a2 | bc -l)
	delta_post=$(echo $b-$a1 | bc -l)
	echo "RESULT: status=$key|delta post=$delta_post ms|delta fetch=$delta_fetch ms"
done

echo "DONE."

