from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os

if len(sys.argv) < 2:
	print "Usage: <device name>"
	sys.exit(1)
 
deviceName = sys.argv[1]


if not os.path.exists("screenshots"):
    print "creating the screenshots directory"
    os.makedirs("screenshots")

# connection to the current device, and return a MonkeyDevice object
#devices = os.popen('adb devices').read().strip().split('\n')[1:]
device = MonkeyRunner.waitForConnection(100, deviceName) 
#device = MonkeyRunner.waitForConnection()



'''
apk_path = device.shell('pm path com.vogella.android.test.simpleactivity')
if apk_path.startswith('package:'):
    print "application installed."
else:
    print "not installed, install APK"
    device.installPackage('com.vogella.android.test.simpleactivity.apk')
'''
print "Walking device %s ..." % deviceName
try:
	device.wake()
except:
  #device.shell('killall com.android.commands.monkey')
	print ""


MonkeyRunner.sleep(2)

try:
	device.wake()
except:
  #device.shell('killall com.android.commands.monkey')
	print ""


# drag the unlock-ring (from bottom to center (HTC One X))
print "Unlocking device ..."
device.drag((354, 1191), (324, 525), 1, 10) #Nexus 5
MonkeyRunner.sleep(1)
device.drag((600, 1300), (600, 300), 1, 10) #Nexus 7


#device.press("DPAD_CENTER", MonkeyDevice.DOWN_AND_UP)

#print "starting application...."
#device.startActivity(component='com.facebook.katana/com.facebook.katana.activity.FbMainTabActivity')

#screenshot
#MonkeyRunner.sleep(10)
#result = device.takeSnapshot()
#result.writeToFile('./screenshots/splash.png','png')
#print "screenshot taken and stored on device"

#sending an event which simulate a click on the menu button
#print "Press ..."
#for i in range (0,10):
#   print "i=%s"%i
#device.touch(150,1700,'DOWN_AND_UP')


#MonkeyRunner.sleep(10)


print "Quit ..." 
