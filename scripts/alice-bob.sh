#!/bin/bash

source constants.sh

#ALICE="0a68dcd6"
#BOB="0a484ec7"


if [ $# -lt 2 ]; then
	echo "Usage: <Alice device ID, eg, 0a68dcd6> <Bob device ID, eg, 0a484ec7>"
	exit 1
fi

echo "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20" > keys.txt
ALICE=$1
BOB=$2

echo "Starting Alice ..."
bash run.sh alice $ALICE > alice.log 2>&1 &
#aliceP=$!

#wait for BOB
sleep 7s

#if [ $1 == '1' ]; then
#	echo "Starting Bob: no refreshing version ..."
#	bash run.sh bob-norefresh $BOB > bob-norefresh.log 2>&1 
#else
echo "Starting Bob: with refreshing version ..."
bash run.sh bob $BOB > bob.log 2>&1 
#fi


sleep 1s
#Wait for Bob to finish
#kill $aliceP
