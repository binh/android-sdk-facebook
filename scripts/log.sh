#!/bin/bash
source constants.sh
if [ $1 == alice ]; then
	adb -s $ALICE logcat | grep -i alice
else
	adb -s $BOB logcat | grep -i bob
fi 
