#!/bin/bash
source constants.sh

PKG_NAME="android.support.v7.appcompat.test"
INSTRUMENTATION="android.support.v7.appcompat.test/android.test.InstrumentationTestRunner"


if [ $# -lt 2 ]; then
	echo "Usage: <client name, eg, alice> <device/emulator code, eg, 0a68dcd6>"
	exit 1
fi

client=$1
device=$2

function timestamp {
	echo $(date +"%Y-%m-%d %H:%M:%S %Z:: ")
}

echo "$(timestamp) Re-installing $client.apk testing app onto $device ..."
adb -s $device uninstall $PKG_NAME

if [ $client == "bob-norefresh" ]; then
	cp ../test-blackbox/$client/bin/testBobNoRefresh.apk $client.apk
else
	cp ../test-blackbox/$client/bin/test*.apk $client.apk
fi
#adb -s $device install -f $client.apk

echo "$(timestamp) Wake up device $device ..."
monkeyrunner wakeup-monkey.py $device
if [ $? != 0 ]; then
	echo "$(timestamp) wakeup device FAILED!"
	exit 1
fi


#echo $instrumentation
adb -s $device push log_tag.txt /mnt/sdcard/
if [ $? != 0 ]; then
	echo "Pushing file to $device failed."
	exit 1
fi	


echo "Push keys to $device ..."
adb -s $device push keys.txt /mnt/sdcard/
if [ $? != 0 ]; then
		echo "Pushing file to $device failed."
		exit 1
fi	

if [ $client == alice ]; then
	echo "Push a photo to $device ..."
	adb -s $device push utah-1600KB.jpg /sdcard/Pictures/utah.jpg
	if [ $? != 0 ]; then
		echo "Pushing file to $device failed."
		exit 1
	fi	
fi

#Run the test
echo "$(timestamp) Collecting log, in $client.logcat ..."
rm $client.logcat
logcat=$(cat log_tag.txt)
#adb -s $device logcat |  awk '$0 ~/AT@/ {print strftime("%s,"), $0; fflush();}' | grep $logcat > $client.logcat 2>&1 &
adb -s $device logcat | while IFS= read -r line; do if [[ $line == *"AT@"* ]]; then printf '%s: %s\n' "$(gdate '+%s%N' | cut -b1-13)" "$line"; fi done > $client.logcat 2>&1 &
#adb -s $device logcat | grep $logcat | grep -i "$client"  > $client.logcat 2>&1 & 
#logcat=$!

echo "$(timestamp) Run instrumentation $INSTRUMENTATION ..."
adb -s $device shell am instrument -w $INSTRUMENTATION

#kill $logcat
logcat=$(ps ax | grep adb | grep $device | cut -d" " -f1)
kill $logcat
echo "$(timestamp) DONE. Results are in $client.logcat"

